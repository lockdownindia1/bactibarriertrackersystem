from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, HttpResponse
from django.urls import reverse
from .models import CustomUser, FranchiseUser, ServiceType, ServiceDetail, ClientUser, AdminUser, StateName, DistrictName
from time import strftime
import random
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from io import StringIO, BytesIO
import os
from django.conf import settings

def AdminHomePage(request):
    context ={}
    currentUser_custom = CustomUser.objects.get(id=request.user.id)
    checkUser = AdminUser.objects.filter(admin__id=request.user.id)
    if len(checkUser) > 0:
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)
    else:
        create_currentUser_admin = AdminUser(admin=currentUser_custom)
        create_currentUser_admin.save()
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)

    context['currentUser_admin'] = currentUser_admin
    return render(request, "Admin_Templates/home_admin.html", context)


def AdminEditProfile(request):
    context = {}
    stateList = StateName.objects.all()
    context['stateList']=stateList
    currentUser_custom = CustomUser.objects.get(id=request.user.id)
    checkUser = AdminUser.objects.filter(admin__id=request.user.id)
    if len(checkUser) > 0:
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)
    else:
        create_currentUser_admin = AdminUser(admin=currentUser_custom)
        create_currentUser_admin.save()
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)

    if request.method == 'POST':
        currentUser_custom.first_name = request.POST["fname"]
        currentUser_custom.last_name = request.POST['lname']
        currentUser_custom.email = request.POST['email']
        currentUser_custom.save()
        if "userpic" in request.FILES:
            currentUser_admin.profilePic = request.FILES['userpic']
        currentUser_admin.phoneNumber = request.POST['phone']
        currentUser_admin.aadharNumber = request.POST['aadhar']
        currentUser_admin.gender = request.POST['gender']
        currentUser_admin.address = request.POST['address']
        currentUser_admin.state = request.POST['state']
        currentUser_admin.district = request.POST['district']
        currentUser_admin.city = request.POST['city']
        currentUser_admin.pinCode = request.POST['pincode']
        currentUser_admin.save()
        context['status'] = 'Profile Updated Successfully'
    context['currentUser_custom'] = currentUser_custom
    context['currentUser_admin'] = currentUser_admin
    return render(request, "Admin_Templates/edit_admin_profile.html", context)



def AdminViewProfile(request):
    context = {}
    currentUser_custom = CustomUser.objects.get(id=request.user.id)
    checkUser = AdminUser.objects.filter(admin__id=request.user.id)
    if len(checkUser) > 0:
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)
    else:
        create_currentUser_admin = AdminUser(admin=currentUser_custom)
        create_currentUser_admin.save()
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)
    context['currentUser_admin'] = currentUser_admin
    return render(request, "Admin_Templates/view_admin_profile.html", context)


def AdminAddFranchise(request):
    context = {}
    stateList = StateName.objects.all()
    context['stateList']=stateList
    currentUser_custom = CustomUser.objects.get(id=request.user.id)
    checkUser = AdminUser.objects.filter(admin__id=request.user.id)
    if len(checkUser) > 0:
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)
    else:
        create_currentUser_admin = AdminUser(admin=currentUser_custom)
        create_currentUser_admin.save()
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)

    context['currentUser_admin'] = currentUser_admin

    if request.method == 'POST':
        first_name = request.POST["fname"]
        last_name = request.POST['lname']
        phoneNumber = request.POST['phone']
        email = request.POST['email']
        gender = request.POST['gender']
        aadharNumber = request.POST['aadhar']
        address = request.POST['address']
        state = request.POST['state']
        district = request.POST['district']
        city = request.POST['city']
        pinCode = request.POST['pincode']
        while(1):
            username = first_name.lower() + '_' + last_name.lower() + strftime("%H%M%S")
            if not CustomUser.objects.filter(username=username).exists():
                break
        password = strftime("%M%S") + str(random.randint(100,999)) + strftime("%d%m")
        newUser = CustomUser.objects.create_user(username=username,password=password,email=email,first_name=first_name,last_name=last_name, user_type=2)
        newUser.is_staff = True
        admin_obj = AdminUser.objects.get(admin__id=request.user.id)
        newUser.franchiseuser.adminID = admin_obj
        newUser.franchiseuser.phoneNumber = phoneNumber
        newUser.franchiseuser.gender = gender
        newUser.franchiseuser.aadharNumber = aadharNumber
        newUser.franchiseuser.address = address
        newUser.franchiseuser.state = state
        newUser.franchiseuser.district = district
        newUser.franchiseuser.city = city
        newUser.franchiseuser.pinCode = pinCode
        if "userpic" in request.FILES:
            newUser.franchiseuser.profilePic = request.FILES['userpic']
        if "aadharfile" in request.FILES:
            newUser.franchiseuser.aadharDoc = request.FILES['aadharfile']
        newUser.save()
        sub = "Account Creation"
        msg = "Hi " + first_name + ',\n\nYour Franchise account created below are details.\n\nUsername - ' + username + '\nPassword - ' + password + '\n\nThanks\nTeam'
        try:
            em = EmailMessage(sub, msg, to=[email, ])
            em.send()
            messages.info(request, 'Account Created Successfully and Mail Sent to Created Franchise')
        except:
            messages.info(request, 'Account Created Successfully but mail not send due to technical issue. USERNAME of created account is - ' + username)

    return render(request, "Admin_Templates/add_franchise.html", context)

def AdminAllFranchise(request):
    context ={}
    currentUser_custom = CustomUser.objects.get(id=request.user.id)
    checkUser = AdminUser.objects.filter(admin__id=request.user.id)
    if len(checkUser) > 0:
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)
    else:
        create_currentUser_admin = AdminUser(admin=currentUser_custom)
        create_currentUser_admin.save()
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)
    context['currentUser_admin'] = currentUser_admin
    franchises=FranchiseUser.objects.filter(adminID=currentUser_admin.id)
    print(franchises)
    context['franchises'] = franchises
    return render(request,"Admin_Templates/view_franchise_list.html",context)

def AdminUpdateFAQ(request):
    context ={}
    currentUser_custom = CustomUser.objects.get(id=request.user.id)
    checkUser = AdminUser.objects.filter(admin__id=request.user.id)
    if len(checkUser) > 0:
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)
    else:
        create_currentUser_admin = AdminUser(admin=currentUser_custom)
        create_currentUser_admin.save()
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)
    context['currentUser_admin'] = currentUser_admin
    return render(request,"Admin_Templates/admin_updateFAQ.html",context)


def link_callback(uri, rel):
    """
    Convert HTML URIs to absolute system paths so xhtml2pdf can access those
    resources
    """
    # use short variable names
    sUrl = settings.STATIC_URL      # Typically /static/
    sRoot = settings.STATIC_ROOT    # Typically /home/userX/project_static/
    mUrl = settings.MEDIA_URL       # Typically /static/media/
    mRoot = settings.MEDIA_ROOT     # Typically /home/userX/project_static/media/
    # convert URIs to absolute system paths
    if uri.startswith(mUrl):
        path = os.path.join(mRoot, uri.replace(mUrl, ""))
    elif uri.startswith(sUrl):
        path = os.path.join(sRoot, uri.replace(sUrl, ""))
    else:
        return uri  # handle absolute uri (ie: http://some.tld/foo.png)
    # make sure that file exists
    if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
    return path

def DigitalID(request, franch_id):
    franchises=FranchiseUser.objects.filter(id=franch_id)
    # clients=ClientUser.objects.all()
    data={'franchises':franchises}
    template=get_template("Digital-ID.html")
    data_p=template.render(data)
    response=BytesIO()
    pdfPage=pisa.pisaDocument(BytesIO(data_p.encode("UTF-8")),response, link_callback=link_callback)
    if not pdfPage.err:
        return HttpResponse(response.getvalue(),content_type="application/pdf")
    else:
        return HttpResponse("Error Generating PDF")


def AdminCertificateView(request):
    context ={}
    currentUser_custom = CustomUser.objects.get(id=request.user.id)
    checkUser = AdminUser.objects.filter(admin__id=request.user.id)
    if len(checkUser) > 0:
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)
    context['currentUser_admin'] = currentUser_admin
    clients=ClientUser.objects.all()
    context['clients'] = clients
    return render(request,"Admin_Templates/view_clients_list.html",context)

def AdminClientCertificate(request, client_id):
    context ={}
    currentUser_custom = CustomUser.objects.get(id=request.user.id)
    checkUser = AdminUser.objects.filter(admin__id=request.user.id)
    if len(checkUser) > 0:
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)
    context['currentUser_admin'] = currentUser_admin
    clients=ClientUser.objects.filter(id=client_id)
    data={'clients':clients}
    template=get_template("Admin_Templates/certificate-view.html")
    data_p=template.render(data)
    response=BytesIO()
    pdfPage=pisa.pisaDocument(BytesIO(data_p.encode("UTF-8")),response, link_callback=link_callback)
    if not pdfPage.err:
        return HttpResponse(response.getvalue(),content_type="application/pdf")
    else:
        return HttpResponse("Error Generating PDF")
