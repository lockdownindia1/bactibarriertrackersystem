from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.shortcuts import HttpResponse
from django.http import JsonResponse
from django.core.mail import EmailMessage
import random
from .models import CustomUser

def forgotpassword(request):
    context= {}
    if request.method == "POST":
        uname = request.POST['uname']
        newpass = request.POST['newpass']
        user = get_object_or_404(CustomUser, username=uname)
        user.set_password(newpass)
        user.save()
        context["status"]= "Password changed Successfully!!!!!"
    return render(request, 'forgotpassword.html', context)

def resetpassword(request):
    un = request.GET["username"]
    try:
        user = get_object_or_404(CustomUser, username = un)
        otp = random.randint(1000,9999)
        msz = "Dear {} \n\n{} is your One Time Password (OTP).\nDon't share it with other\n\nThanks\nTeam".format(user.username, otp)
        try:
            email = EmailMessage("Account Verification", msz,to=[user.email])
            email.send()
            return JsonResponse({"status": "sent", "email": user.email, 'rotp':otp})
        except:
            return JsonResponse({"status": "error"})
    except:
        return JsonResponse({"status":"failed"})

