from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.deprecation import MiddlewareMixin


class LoginCheckMiddleWare(MiddlewareMixin):

    def process_view(self,request,view_func,view_args,view_kwargs):
        modulename=view_func.__module__
        # print("Module: ", modulename)
        user=request.user
        if modulename == 'django.contrib.admin.sites':
            pass
        elif modulename == "BactiBarrierTrackerApp.ForgotPasswordViews":
            pass
        elif user.is_authenticated:
            if modulename == 'django.views.static':
                pass
            elif user.user_type == "1":
                if modulename == "BactiBarrierTrackerApp.AdminViews":
                    pass
                elif modulename == "BactiBarrierTrackerApp.views":
                    pass
                elif modulename == "BactiBarrierTrackerApp.EmailViews":
                    pass
                elif request.path == "BactiBarrierTrackerSystem.media":
                    pass
                else:
                    return redirect("AdminHomePage")
            elif user.user_type == "2":
                if modulename == "BactiBarrierTrackerApp.FranchiseViews":
                    pass
                elif modulename == "BactiBarrierTrackerApp.views":
                    pass
                else:
                    return HttpResponseRedirect(reverse("FranchiseHomePage"))
            elif user.user_type == "3":
                if modulename == "BactiBarrierTrackerApp.ClientViews":
                    pass
                elif modulename == "BactiBarrierTrackerApp.views":
                    pass
                else:
                    return HttpResponseRedirect(reverse("ClientHomePage"))
            else:
                return HttpResponseRedirect(reverse("LoginPage"))
        else:
            if request.path == reverse("LoginPage"):
                pass
            else:
                return HttpResponseRedirect(reverse("LoginPage"))
