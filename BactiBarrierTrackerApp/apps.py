from django.apps import AppConfig


class BactibarriertrackerappConfig(AppConfig):
    name = 'BactiBarrierTrackerApp'
