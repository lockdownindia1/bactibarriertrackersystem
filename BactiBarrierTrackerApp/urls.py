from django.urls import path
from . import views, AdminViews, FranchiseViews, ClientViews, EmailViews, ForgotPasswordViews

urlpatterns = [
    path('', views.LoginPage, name="LoginPage"),
    path('forgotpassword', ForgotPasswordViews.forgotpassword, name="forgotpassword"),
    path('resetpassword', ForgotPasswordViews.resetpassword, name="resetpassword"),
    path('LogoutUser', views.LogoutUser, name="LogoutUser"),

    path('AdminHomePage', AdminViews.AdminHomePage, name="AdminHomePage"),
    path('AdminEditProfile', AdminViews.AdminEditProfile, name="AdminEditProfile"),
    path('AdminViewProfile', AdminViews.AdminViewProfile, name="AdminViewProfile"),
    path('AdminAddFranchise', AdminViews.AdminAddFranchise, name="AdminAddFranchise"),
    path('AdminAllFranchise', AdminViews.AdminAllFranchise, name="AdminAllFranchise"),
    path('AdminUpdateFAQ', AdminViews.AdminUpdateFAQ, name="AdminUpdateFAQ"),

    path('DigitalID/<str:franch_id>', AdminViews.DigitalID, name="DigitalID"),
    path('AdminCertificateView', AdminViews.AdminCertificateView, name="AdminCertificateView"),
    path('AdminClientCertificate/<str:client_id>', AdminViews.AdminClientCertificate, name="AdminClientCertificate"),
    path('ChangePassword', views.ChangePassword, name="ChangePassword"),
    path('SendEmail', EmailViews.SendEmail, name="SendEmail"),
    path('FranchiseHomePage', FranchiseViews.FranchiseHomePage, name="FranchiseHomePage"),
    path('FranchiseViewProfile', FranchiseViews.FranchiseViewProfile, name="FranchiseViewProfile"),
    path('FranchiseEditProfile', FranchiseViews.FranchiseEditProfile, name="FranchiseEditProfile"),
    path('FranchiseCertificateIssue', FranchiseViews.FranchiseCertificateIssue, name="FranchiseCertificateIssue"),
    path('FranchiseCertificateView', FranchiseViews.FranchiseCertificateView, name="FranchiseCertificateView"),
    path('ClientCertificate/<str:client_id>', FranchiseViews.ClientCertificate, name="ClientCertificate"),
    path('FranchiseDigitalID', FranchiseViews.FranchiseDigitalID, name="FranchiseDigitalID"),

    path('depandent_District', views.depandent_District, name="depandent_District"),
    path('ClientHomePage', ClientViews.ClientHomePage, name="ClientHomePage"),
]
