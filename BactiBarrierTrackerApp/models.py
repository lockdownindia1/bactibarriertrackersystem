from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver

class CustomUser(AbstractUser):
    user_type_data=(("1", "Admin"),("2", "Franchise"),("3", "Client"))
    user_type=models.CharField(default=1,choices=user_type_data,max_length=10)
    class Meta:
        db_table = "CustomUser"

class AdminUser(models.Model):
    id=models.AutoField(primary_key=True)
    admin=models.OneToOneField(CustomUser,on_delete=models.CASCADE)
    profilePic = models.ImageField(upload_to='profile_pic', blank=True)
    phoneNumber = models.IntegerField()
    gender = models.CharField(max_length=6)
    aadharNumber = models.IntegerField()
    address = models.CharField(max_length=255)
    state = models.CharField(max_length=155)
    district = models.CharField(max_length=155)
    city = models.CharField(max_length=155)
    pinCode = models.IntegerField()
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now_add=True)
    objects=models.Manager()
    class Meta:
        db_table = "AdminUser"
    def __str__(self):
        return self.admin.username


class FranchiseUser(models.Model):
    id=models.AutoField(primary_key=True)
    admin=models.OneToOneField(CustomUser,on_delete=models.CASCADE)
    adminID=models.ForeignKey(AdminUser,on_delete=models.CASCADE,default=1)
    profilePic = models.ImageField(upload_to='profile_pic', blank=True)
    aadharDoc = models.ImageField(upload_to='profile_pic', blank=True)
    phoneNumber = models.IntegerField()
    gender = models.CharField(max_length=6)
    aadharNumber = models.IntegerField()
    address = models.CharField(max_length=255)
    state = models.CharField(max_length=155)
    district = models.CharField(max_length=155)
    city = models.CharField(max_length=155)
    pinCode = models.IntegerField()
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now_add=True)
    objects=models.Manager()
    class Meta:
        db_table = "FranchiseUser"
    def __str__(self):
        return self.admin.username


class ServiceType(models.Model):
    id=models.AutoField(primary_key=True)
    service_type=models.CharField(max_length=255, default=1)
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now_add=True)
    objects=models.Manager()
    class Meta:
        db_table = "ServiceType"

class ServiceDetail(models.Model):
    id=models.AutoField(primary_key=True)
    service_name=models.CharField(max_length=255)
    servicetype_id=models.ForeignKey(ServiceType,on_delete=models.CASCADE,default=1)
    franchise_id=models.ForeignKey(CustomUser,on_delete=models.CASCADE)
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now_add=True)
    objects=models.Manager()
    class Meta:
        db_table = "ServiceDetail"


class ClientUser(models.Model):
    id=models.AutoField(primary_key=True)
    admin=models.OneToOneField(CustomUser,on_delete=models.CASCADE)
    franchiseID=models.ForeignKey(FranchiseUser,on_delete=models.CASCADE, default=1)
    # servicetype_id=models.ForeignKey(ServiceType,on_delete=models.DO_NOTHING)
    profilePic = models.ImageField(upload_to='profile_pic', blank=True)
    aadharDoc = models.ImageField(upload_to='aadhar_pic', blank=True)
    policeDoc = models.ImageField(upload_to='police_pic', blank=True)
    barcodePath = models.CharField(max_length=6)
    phoneNumber = models.IntegerField()
    gender = models.CharField(max_length=6)
    aadharNumber = models.IntegerField()
    address = models.CharField(max_length=255)
    state = models.CharField(max_length=155)
    district = models.CharField(max_length=155)
    city = models.CharField(max_length=155)
    pinCode = models.IntegerField()
    # number_of_service=models.IntegerField()
    # service_start_date=models.DateTimeField()
    # service_end_date=models.DateTimeField()
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now_add=True)
    objects = models.Manager()
    class Meta:
        db_table = "ClientUser"
    def __str__(self):
        return self.admin.username


class FeedBackFranchise(models.Model):
    id = models.AutoField(primary_key=True)
    franchise_id=models.ForeignKey(ClientUser,on_delete=models.CASCADE)
    feedback = models.TextField()
    feedback_reply = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()
    class Meta:
        db_table = "FeedBackranchise"

class FeedBackClient(models.Model):
    id = models.AutoField(primary_key=True)
    client_id = models.ForeignKey(FranchiseUser, on_delete=models.CASCADE)
    feedback = models.TextField()
    feedback_reply=models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()
    class Meta:
        db_table = "FeedBackClient"

class NotificationFranchise(models.Model):
    id = models.AutoField(primary_key=True)
    franchise_id = models.ForeignKey(FranchiseUser, on_delete=models.CASCADE)
    message = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()
    class Meta:
        db_table = "NotificationFranchise"

class NotificationClient(models.Model):
    id = models.AutoField(primary_key=True)
    client_id = models.ForeignKey(ClientUser, on_delete=models.CASCADE)
    message = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()
    class Meta:
        db_table = "NotificationClient"

class EmailSend(models.Model):
    id = models.AutoField(primary_key=True)
    email_subject = models.CharField(max_length=255)
    email_from = models.CharField(max_length=255)
    email_to = models.CharField(max_length=255)
    email_body = models.CharField(max_length=255)
    admin=models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    email_Status = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()
    class Meta:
        db_table = "EmailSend"


class StateName(models.Model):
    id = models.AutoField(primary_key=True)
    state_name = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()
    class Meta:
        db_table = "StateName"
    def __str__(self):
        return self.state_name

class DistrictName(models.Model):
    id = models.AutoField(primary_key=True)
    district_name = models.CharField(max_length=255)
    state_id=models.ForeignKey(StateName,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    objects = models.Manager()
    class Meta:
        db_table = "DistrictName"
    def __str__(self):
        return self.district_name

@receiver(post_save,sender=CustomUser)
def create_user_profile(sender,instance,created,**kwargs):
    if created:
        if instance.user_type==1:
            AdminUser.objects.create(admin=instance,profilePic="",phoneNumber=None,gender="",aadharNumber=None,address="",state="",district="",city="",pinCode=None)
        if instance.user_type==2:
            FranchiseUser.objects.create(admin=instance,adminID=AdminUser.objects.get(id=1),profilePic="",aadharDoc="",phoneNumber=None,gender="",aadharNumber=None,address="",state="",district="",city="",pinCode=None)
        if instance.user_type==3:
            ClientUser.objects.create(admin=instance,franchiseID=FranchiseUser.objects.get(id=1),profilePic="",aadharDoc="",policeDoc="",phoneNumber=None,gender="",aadharNumber=None,address="",state="",district="",city="",pinCode=None,number_of_service=1,service_start_date="2020-01-01",service_end_date="2021-01-01")

@receiver(post_save,sender=CustomUser)
def save_user_profile(sender,instance,**kwargs):
    if instance.user_type==1:
        instance.adminuser.save()
    if instance.user_type==2:
        instance.franchiseuser.save()
    if instance.user_type==3:
        instance.clientuser.save()
