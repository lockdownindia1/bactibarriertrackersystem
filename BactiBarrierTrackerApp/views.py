from django.contrib import auth, messages
from django.shortcuts import render, redirect
from django.urls import reverse
from .models import CustomUser
from django.shortcuts import HttpResponse
from .models import StateName, DistrictName, AdminUser, FranchiseUser, ClientUser
from django.conf import settings
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from io import StringIO, BytesIO
import os
from django.conf import settings


def LoginPage(request):
    if request.method == 'POST':
        txtCaptcha = request.POST['txtCaptcha']
        genCaptcha = request.POST['genCaptcha']
        genCaptcha = genCaptcha.replace(' ', '')
        print(genCaptcha)
        if txtCaptcha == genCaptcha:
            username = request.POST['username']
            password = request.POST['password']
            user = auth.authenticate(request, username=username, password=password)
            if user is not None:
                auth.login(request, user)
                if user.user_type=="1":
                    return redirect('AdminHomePage')
                elif user.user_type=="2":
                    return redirect('FranchiseHomePage')
                elif user.user_type == "3":
                    return redirect('ClientHomePage')
                else:
                    messages.info(request, 'Wrong Credentials')
                    return render(request, "Login-Page.html")
            else:
                messages.info(request, 'Wrong Credentials')
                return render(request, "Login-Page.html")
        else:
            messages.info(request, 'Wrong Captach')
            username = request.POST['username']
            return render(request, "Login-Page.html", {'username':username})
    else:
        return render(request, "Login-Page.html")

def LogoutUser(request):
    auth.logout(request)
    return redirect('/')


def ChangePassword(request):
    context={}
    user = CustomUser.objects.get(id=request.user.id)
    if request.method == "POST":
        currentPass = request.POST["cpass"]
        newPass = request.POST["pass"]
        check = user.check_password(currentPass)
        if check==True:
            user.set_password(newPass)
            user.save()
            context["msg"] = "Password Changed Successfully"
        else:
            context["msg"] = "Enter current password is wrong"

    if user.user_type == "1":
        return render(request, 'change-password.html', context)
    elif user.user_type == "2":
        return render(request, 'change-password1.html', context)
    elif user.user_type == "3":
        return render(request, 'change-password2.html', context)

def depandent_District(request):
    if request.method == 'GET':
        sName = request.GET['sN']
        stateObject = StateName.objects.get(state_name=sName)
        distName = DistrictName.objects.filter(state_id=stateObject)
        distList = '<option value="0" selected>--Select District--</option>'
        for d in distName:
            distList = distList + '<option value="' + d.district_name + '">' + d.district_name + '</option>'
        return HttpResponse(distList)

