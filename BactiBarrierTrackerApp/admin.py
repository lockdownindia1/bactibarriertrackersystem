from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser, AdminUser, FranchiseUser, ClientUser, ServiceType, ServiceDetail, FeedBackClient, FeedBackFranchise, NotificationClient, NotificationFranchise, EmailSend, StateName, DistrictName

class UserModel(UserAdmin):
    fieldsets = (
        *UserAdmin.fieldsets,  # original form fieldsets, expanded
        (  # new fieldset added on to the bottom
            'Custom Field Heading',  # group heading of your choice; set to None for a blank space instead of a header
            {
                'fields': (
                    'user_type',
                ),
            },
        ),
    )
admin.site.register(CustomUser, UserModel)
admin.site.register(AdminUser)
admin.site.register(FranchiseUser)
admin.site.register(ClientUser)
admin.site.register(ServiceType)
admin.site.register(ServiceDetail)
admin.site.register(FeedBackClient)
admin.site.register(FeedBackFranchise)
admin.site.register(NotificationClient)
admin.site.register(NotificationFranchise)
admin.site.register(EmailSend)

admin.site.register(StateName)
admin.site.register(DistrictName)
