from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.core.mail import EmailMessage
from .models import CustomUser, AdminUser, EmailSend

def SendEmail(request):
    context = {}
    currentUser_custom = CustomUser.objects.get(id=request.user.id)
    checkUser = AdminUser.objects.filter(admin__id=request.user.id)
    if len(checkUser) > 0:
        currentUser_admin = AdminUser.objects.get(admin__id=request.user.id)
    context['currentUser_admin'] = currentUser_admin

    if request.method == 'POST':
        smail = request.POST['toemail']
        sub = request.POST['subject']
        msg = request.POST['messagebody']
        try:
            em = EmailMessage(sub, msg, to=[smail, ])
            em.send()
            context['status'] = 'Email sent'
            emailObject = EmailSend.objects.create(admin=currentUser_custom,email_subject=sub, email_to=smail,
                                                   email_body=msg, email_Status="Send")
            emailObject.save()
            redirect('AdminHomePage')
        except:
            emailObject = EmailSend.objects.create(admin=currentUser_custom,email_subject=sub, email_to=smail,
                                                   email_body=msg, email_Status="Not Send")
            emailObject.save()
            context['status'] = 'Could not send, please send again'

    return render(request, 'Admin_Templates/email_send.html', context)
