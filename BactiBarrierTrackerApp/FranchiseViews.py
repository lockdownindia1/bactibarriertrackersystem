from time import strftime
from django.core.mail import EmailMessage
from django.shortcuts import render, redirect
from .models import FranchiseUser, CustomUser, AdminUser, ClientUser, StateName
from django.contrib import messages
import random
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from io import StringIO, BytesIO
import os
from django.conf import settings

def link_callback(uri, rel):
    """
    Convert HTML URIs to absolute system paths so xhtml2pdf can access those
    resources
    """
    # use short variable names
    sUrl = settings.STATIC_URL      # Typically /static/
    sRoot = settings.STATIC_ROOT    # Typically /home/userX/project_static/
    mUrl = settings.MEDIA_URL       # Typically /static/media/
    mRoot = settings.MEDIA_ROOT     # Typically /home/userX/project_static/media/
    # convert URIs to absolute system paths
    if uri.startswith(mUrl):
        path = os.path.join(mRoot, uri.replace(mUrl, ""))
    elif uri.startswith(sUrl):
        path = os.path.join(sRoot, uri.replace(sUrl, ""))
    else:
        return uri  # handle absolute uri (ie: http://some.tld/foo.png)
    # make sure that file exists
    if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
    return path

# PDF generater
def ClientCertificate(request, client_id):
    currentUser_franchise = FranchiseUser.objects.get(admin__id=request.user.id)
    clients=ClientUser.objects.filter(id=client_id)
    # clients=ClientUser.objects.all()
    data={'clients':clients, 'currentUser_franchise':currentUser_franchise}
    template=get_template("certificate-view.html")
    data_p=template.render(data)
    response=BytesIO()
    pdfPage=pisa.pisaDocument(BytesIO(data_p.encode("UTF-8")),response, link_callback=link_callback)
    if not pdfPage.err:
        return HttpResponse(response.getvalue(),content_type="application/pdf")
    else:
        return HttpResponse("Error Generating PDF")


def FranchiseHomePage(request):
    context ={}
    currentUser_custom = CustomUser.objects.get(id=request.user.id)
    checkUser = FranchiseUser.objects.filter(admin__id=request.user.id)
    if len(checkUser) > 0:
        currentUser_franchise = FranchiseUser.objects.get(admin__id=request.user.id)
    else:
        create_currentUser_franchise = FranchiseUser(admin=currentUser_custom)
        create_currentUser_franchise.save()
        currentUser_franchise = FranchiseUser.objects.get(admin__id=request.user.id)

    context['currentUser_franchise'] = currentUser_franchise
    return render(request, "Franchise_Templates/home_franchise.html", context)

def FranchiseViewProfile(request):
    context = {}
    currentUser_custom = CustomUser.objects.get(id=request.user.id)
    checkUser = FranchiseUser.objects.filter(admin__id=request.user.id)
    if len(checkUser) > 0:
        currentUser_franchise = FranchiseUser.objects.get(admin__id=request.user.id)
    else:
        create_currentUser_franchise = FranchiseUser(admin=currentUser_custom)
        create_currentUser_franchise.save()
        currentUser_franchise = FranchiseUser.objects.get(admin__id=request.user.id)
    context['currentUser_franchise'] = currentUser_franchise
    return render(request, "Franchise_Templates/view_franchise_profile.html", context)


def FranchiseEditProfile(request):
    context = {}
    stateList = StateName.objects.all()
    context['stateList']=stateList
    currentUser_custom = CustomUser.objects.get(id=request.user.id)
    checkUser = FranchiseUser.objects.filter(admin__id=request.user.id)
    if len(checkUser) > 0:
        currentUser_franchise = FranchiseUser.objects.get(admin__id=request.user.id)
    else:
        create_currentUser_franchise = FranchiseUser(admin=currentUser_custom)
        create_currentUser_franchise.save()
        currentUser_franchise = FranchiseUser.objects.get(admin__id=request.user.id)

    if request.method == 'POST':
        currentUser_custom.first_name = request.POST["fname"]
        currentUser_custom.last_name = request.POST['lname']
        currentUser_custom.email = request.POST['email']
        currentUser_custom.save()
        if "userpic" in request.FILES:
            currentUser_franchise.profilePic = request.FILES['userpic']
        currentUser_franchise.phoneNumber = request.POST['phone']
        currentUser_franchise.aadharNumber = request.POST['aadhar']
        currentUser_franchise.gender = request.POST['gender']
        currentUser_franchise.address = request.POST['address']
        currentUser_franchise.state = request.POST['state']
        currentUser_franchise.district = request.POST['district']
        currentUser_franchise.city = request.POST['city']
        currentUser_franchise.pinCode = request.POST['pincode']
        currentUser_franchise.save()
        context['status'] = 'Profile Updated Successfully'
    context['currentUser_custom'] = currentUser_custom
    context['currentUser_franchise'] = currentUser_franchise
    return render(request, "Franchise_Templates/edit_franchise_profile.html", context)


def FranchiseCertificateIssue(request):
    context = {}
    stateList = StateName.objects.all()
    context['stateList']=stateList
    currentUser_custom = CustomUser.objects.get(id=request.user.id)
    checkUser = FranchiseUser.objects.filter(admin__id=request.user.id)
    if len(checkUser) > 0:
        currentUser_franchise = FranchiseUser.objects.get(admin__id=request.user.id)
    else:
        create_currentUser_franchise = FranchiseUser(admin=currentUser_custom)
        create_currentUser_franchise.save()
        currentUser_franchise = FranchiseUser.objects.get(admin__id=request.user.id)
    context['currentUser_franchise'] = currentUser_franchise
    if request.method == 'POST':
        first_name = request.POST["fname"]
        last_name = request.POST['lname']
        phoneNumber = request.POST['phone']
        email = request.POST['email']
        gender = request.POST['gender']
        aadharNumber = request.POST['aadhar']
        address = request.POST['address']
        state = request.POST['state']
        district = request.POST['district']
        city = request.POST['city']
        pinCode = request.POST['pincode']
        while(1):
            username = first_name.lower() + '_' + last_name.lower() + strftime("%H%M%S")
            if not CustomUser.objects.filter(username=username).exists():
                break
        password = strftime("%M%S") + str(random.randint(100,999)) + strftime("%d%m")
        newUser = CustomUser.objects.create_user(username=username,password=password,email=email,first_name=first_name,last_name=last_name,user_type=3)
        newUser.is_active = False
        franchise_obj = FranchiseUser.objects.get(admin__id=request.user.id)
        newUser.clientuser.franchiseID = franchise_obj
        newUser.clientuser.phoneNumber = phoneNumber
        newUser.clientuser.gender = gender
        newUser.clientuser.aadharNumber = aadharNumber
        newUser.clientuser.address = address
        newUser.clientuser.state = state
        newUser.clientuser.district = district
        newUser.clientuser.city = city
        newUser.clientuser.pinCode = pinCode
        if "userpic" in request.FILES:
            newUser.clientuser.profilePic = request.FILES['userpic']
        if "aadharFile" in request.FILES:
            newUser.clientuser.aadharDoc = request.FILES['aadharFile']
        if "policeFile" in request.FILES:
            newUser.clientuser.policeDoc = request.FILES['policeFile']

# QR Code generation
        import qrcode
        import os
        qr = qrcode.QRCode(version=1,box_size=3,border=5)
        data = 'Name:- ' + first_name + ' ' + last_name + '\nPhone Number:- ' + phoneNumber + '\nEmail:- ' + email
        qr.add_data(data)
        img = qr.make_image(fill="black",back_color="white")
        fileNameQRCode = 'static/img/barcode/' + username + '.jpg'
        img.save(fileNameQRCode)
        newUser.clientuser.barcodePath = fileNameQRCode
        newUser.save()

#Email sending
        sub = "Account Creation"
        msg = "Hi " + first_name + ',\n\nYour Service account created below are details.\n\nUsername - ' + username + '\nPassword - ' + password + '\n\nThanks\nTeam'
        try:
            em = EmailMessage(sub, msg, to=[email, ])
            em.send()
            messages.info(request, 'Account Created Successfully and Mail Sent to Created Client')
        except:
            messages.info(request, 'Account Created Successfully but mail not send due to technical issue. USERNAME of created account is - ' + username)
    return render(request, "Franchise_Templates/certificate-issue.html", context)


def FranchiseCertificateView(request):
    context ={}
    currentUser_custom = CustomUser.objects.get(id=request.user.id)
    checkUser = FranchiseUser.objects.filter(admin__id=request.user.id)
    if len(checkUser) > 0:
        currentUser_franchise = FranchiseUser.objects.get(admin__id=request.user.id)
    else:
        create_currentUser_franchise = FranchiseUser(admin=currentUser_custom)
        create_currentUser_franchise.save()
        currentUser_franchise = FranchiseUser.objects.get(admin__id=request.user.id)
    context['currentUser_franchise'] = currentUser_franchise
    clients=ClientUser.objects.filter(franchiseID=currentUser_franchise.id)
    context['clients'] = clients
    return render(request,"Franchise_Templates/view_clients_list.html",context)



def FranchiseDigitalID(request):
    franchises=FranchiseUser.objects.filter(id=request.user.id)
    # clients=ClientUser.objects.all()
    data={'franchises':franchises}
    template=get_template("Digital-ID.html")
    data_p=template.render(data)
    response=BytesIO()
    pdfPage=pisa.pisaDocument(BytesIO(data_p.encode("UTF-8")),response, link_callback=link_callback)
    if not pdfPage.err:
        return HttpResponse(response.getvalue(),content_type="application/pdf")
    else:
        return HttpResponse("Error Generating PDF")
